﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part1
{
    class Calculator
    {
        static int Total;
        static int Number;
        static string Output;

        public static string Calculate(string TxtBox)
        {
            
            if (int.TryParse(TxtBox, out Number))
            {
                for (var i = 0; i < Number+1; i++)
                {
                    if (i % 3 == 0)
                    {
                        Total = i + Total;
                    }
                    else if (i % 5 == 0)
                    {
                        Total = i + Total;
                    }
                    Output = $"The Sum is {Total}";

                }

            }
            else
            {
                Output = "Please Input a valid number";
            }
            Total = 0;
            return Output;
        }
    }
}
