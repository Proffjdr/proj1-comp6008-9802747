﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Part1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public void Click(Object Sender, EventArgs e)
        {
            var txtBox = Input.Text;
            string output = Calculator.Calculate(txtBox);

            Title.Text = output;

        }
    }
}
