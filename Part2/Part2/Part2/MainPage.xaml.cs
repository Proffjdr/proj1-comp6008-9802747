﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Part2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public void Click(Object Sender, EventArgs e)
        {
            int Guess;
            

            if (int.TryParse(Input.Text, out Guess))
            {
                
                
                    bool Outcome = GuessGame.RunGame(Guess);
                

            }
            else
            {
                Title.Text = "Please Input a valid number";
            }
            


            bool Result = GuessGame.RunGame(Guess);
            int Score=GuessGame.TotalScore(Result);
            string Bkgnd = GuessGame.Backgroundcheck(Result);
            ScoreCount.Text = $"{Score}";
            Background.BackgroundColor = Color.FromHex(Bkgnd);
            


        }


    }
}
