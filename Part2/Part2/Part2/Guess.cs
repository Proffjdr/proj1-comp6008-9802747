﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Part2
{
    class GuessGame
    {
        static int Score { get; set; }
       

        public static bool RunGame(int Guess)
        {
            
            Random rnd = new Random();
            int Target = rnd.Next(1, 5);
            bool Result;
                        
              if ( Guess == Target)
                {

                    Result = true;                
                    
                }
                else
                {
                    Result = false;
                }
                       
            return Result;
        }

        public static int TotalScore(bool Input)
        {
            if(Input == true)
            {
                Score++;
            }
            int Output = Score;
            return Output;       
        }

        public static string Backgroundcheck(bool Input)
        {
            string Bkgnd;

            if (Input == true)
            {
                Bkgnd = "00FF00";
            }
            else
            {
                Bkgnd = "FF0000";
            }
            return Bkgnd;
        }
    } 
}
